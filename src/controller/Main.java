package controller;

import model.*;
import view.ViewBoardEditor;
import view.ViewStart;

public class Main {
	private static boolean waiting;
	private static BoardLayout[] boardLayouts;
	private static ViewStart start;
	private static ButtonController buttonController;
	private static final LoadController loadController = new LoadController();
	
	public static void main(String[] args) {
		buttonController = new ButtonController();
		
		boardLayouts = loadController.loadLayouts2();
		start = new ViewStart(boardLayouts, buttonController);
		waiting = true;

		while (waiting) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				System.out.println("Cannot put main thread to sleep");
				e.printStackTrace();
			}
		}
	}

	public static void showStart() {
		boardLayouts = loadController.loadLayouts2();
		start.setLayouts(boardLayouts);
		start.setVisible(true);
	}

	public static void startLayoutEditor() {
		start.setVisible(false);
		new ViewBoardEditor(new LayoutEditorController());
	}
}
