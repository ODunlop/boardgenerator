package view;

import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.ButtonController;
import model.pieces.PieceInterface;

/**
 * This is the panel located on the right (east) side of the main interface and is responsible for showing the timer and the selected piece's details.
 *
 */
public class ViewTimer extends JPanel implements Observer {
    private static final long serialVersionUID = -2178376943831400894L; 
    private JLabel timer;
    private JLabel selectedPieceName;
    private JLabel health;
    private JLabel strength;
    private JLabel defense;
    
    /**
     * Creates a new timer JPanel
     */
    public ViewTimer() {
    	Box box = Box.createVerticalBox();
    	timer = new JLabel("Time: 00");
    	timer.setFont(new Font("Sans-Serif", Font.PLAIN, 18));
    	timer.setBorder(new EmptyBorder(0, 0, 0, 60));
		health = new JLabel();
		strength = new JLabel();
		defense = new JLabel();
		selectedPieceName = new JLabel();
		
		box.add(timer);
		box.add(selectedPieceName);
		box.add(health);
		box.add(strength);
		box.add(defense);
		
		add(box);
    }

	/**
	 * Displays the selected piece's name, health and strength.
	 * 
	 */
	public void addSelectedPiece(ButtonController buttonController, PieceInterface piece) {
		selectedPieceName.setText(piece.getName());
		health.setText("Health: " + piece.getCurrentHealth());
		strength.setText("Strength: " + piece.getStrength());
		defense.setText("Defense: " + piece.getDefense());
		
		selectedPieceName.setVisible(true);
		health.setVisible(true);
		strength.setVisible(true);
	}
	

	/**
	 * Hides the JLabels responsible for showing the selected piece's details
	 */
	public void hideSelected() {
		selectedPieceName.setVisible(false);
		health.setVisible(false);
		strength.setVisible(false);
	}

	@Override
	public void update(Observable o, Object arg) {
		timer.setText("Time: " + (int)arg);
		
	}
}


	