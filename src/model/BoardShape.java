package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Generates an array of Cell objects that are set to the given shape and dimensions
 *
 */
public class BoardShape implements Serializable {
	private static final long serialVersionUID = -5073314603711105062L;
	private String name;
	private int x;
	private int y;
	private Cell cells[][];
	
	/**
	 * Called if the shape is not a circle and has both x and y dimensions
	 * 
	 * @param name - Name of the shape to generate (either Rectangle or Plus)
	 * @param x - The width of the board
	 * @param y - The height of the board
	 */
	public BoardShape(String name, int x, int y) {
		this.name = name;
		this.x = x;
		this.y = y;
		this.cells = new Cell[x][y];
		
		if (name.equals("Plus")) {
			createPlus();
		} else if (name.equals("Rectangle")) {
			createRectangle();
		} else {
			return;
		}
	}
	
	/**
	 * Called if the shape is  a circle and has a radius
	 * 
	 * @param name - The name of the shape (can only be Circle)
	 * @param r - The radius of the circle to generate
	 */
	public BoardShape(String name, int r) {
		if (!name.equals("Circle"))
			return;
		
		this.name = name;
		this.x = (2*r) + 1;
		this.y = (2*r) + 1;
		this.cells = new Cell[x][y];
		
		createCircle();
	}
	
	/**
	 * Generates a plus shape with any given dimensions
	 */
	private void createPlus() {
		int x2 = 0;
		int y2 = 0;
		int isOverX = 0;
		int isOverY = 0;
		int x3 = 0;
		int y3 = 0;
		
		//Works out what the remainder is and stores them when the dimensions are divided by 3
		if (x % 3 == 0) {
			x2 = x/3;
		} else if (x % 3 == 1) {
			x2 = (x-1)/3;
			isOverX = 1;
		} else if (x % 3 == 2) {
			x2 = (x-2)/3;
			isOverX = 2;
		}
		
		if (y % 3 == 0) {
			y2 = y/3;
		} else if (y % 3 == 1) {
			y2 = (y-1)/3;
			isOverY = 1;
		} else if (y % 3 == 2) {
			y2 = (y-2)/3;
			isOverY = 2;
		}
		
		//Initialises the final array
		for (int i = 0; i < x; i++) {
			for (int a = 0; a < y; a++) {
				cells[i][a] = new Cell(i, a, true);
			}
		}
		
		//Creates a list of smaller boolean arrays that represent each of the 9 sections of the plus shape and generates them according to the given dimensions and remainders
		List<boolean[][]> layout = new ArrayList<boolean[][]>();
		
		if (isOverX == 0 && isOverY == 0) {
			for (int i = 0; i < 9; i++) {
				layout.add(new boolean[x2][y2]);
			}
		} else if (isOverX == 0 && isOverY == 1) {
			y3 = 5;
			for (int i = 0; i < 9; i++) {
				if (i == 3 || i == 4 || i == 5) {
					layout.add(new boolean[x2][y2 + 1]);
				} else {
					layout.add(new boolean[x2][y2]);
				}
			}
		} else if (isOverX == 0 && isOverY == 2) {
			y3 = 1;
			for (int i = 0; i < 9; i++) {
				if (i == 1 || i == 4 || i == 7) {
					layout.add(new boolean[x2][y2]);
				} else {
					layout.add(new boolean[x2][y2 + 1]);
				}
			}
		} else if (isOverX == 1 && isOverY == 0) {
			x3 = 5;
			for (int i = 0; i < 9; i++) {
				if (i == 1 || i == 4 || i == 7) {
					layout.add(new boolean[x2 + 1][y2]);
				} else {
					layout.add(new boolean[x2][y2]);
				}
			}
		} else if (isOverX == 1 && isOverY == 1) {
			x3 = 5;
			y3 = 5;
			for (int i = 0; i < 9; i++) {
				if (i == 0 || i == 2 || i == 6 || i == 8) {
					layout.add(new boolean[x2][y2]);
				} else if (i == 1 || i == 7){
					layout.add(new boolean[x2][y2 + 1]);
				} else if (i == 3 || i == 5) {
					layout.add(new boolean[x2 + 1][y2]);
				} else {
					layout.add(new boolean[x2 + 1][y2 + 1]);
				}
			}
		} else if (isOverX == 1 && isOverY == 2) {
			y3++;
			x3 = 5;
			for (int i = 0; i < 9; i++) {
				if (i == 1 || i == 7) {
					layout.add(new boolean[x2 + 1][y2 + 1]);
				} else if (i == 0 || i == 2  || i == 6 || i == 8){
					layout.add(new boolean[x2][y2 + 1]);
				} else if (i == 4) {
					layout.add(new boolean[x2 + 1][y2]);
				} else {
					layout.add(new boolean[x2][y2]);
				}
			}
		} else if (isOverX == 2 && isOverY == 0) {
			x3 = 1;
			for (int i = 0; i < 9; i++) {
				if (i == 1 || i == 4 || i == 7) {
					layout.add(new boolean[x2][y2]);
				} else {
					layout.add(new boolean[x2+1][y2]);
				}
			}
		} else if (isOverX == 2 && isOverY == 1) {
			x3++;
			y3 = 5;
			for (int i = 0; i < 9; i++) {
				if (i == 0 || i == 2 || i == 6 || i == 8) {
					layout.add(new boolean[x2 + 1][y2]);
				} else if (i == 1 || i == 7) {
					layout.add(new boolean[x2][y2]);
				} else if (i == 3 || i == 5) {
					layout.add(new boolean[x2 + 1][y2 + 1]);
				} else {
					layout.add(new boolean[x2][y2 + 1]);
				}
			}
		} else {
			x3++;
			y3++;
			for (int i = 0; i < 9; i++) {
				if (i == 4) {
					layout.add(new boolean[x2][y2]);
				} else if (i == 1 || i == 7) {
					layout.add(new boolean[x2][y2 + 1]);
				} else if (i == 3 || i == 5) {
					layout.add(new boolean[x2 + 1][y2]);
				} else {
					layout.add(new boolean[x2 + 1][y2 + 1]);
				}
			}
		}
		
		//Sets all the cells that are located in the corner arrays to not visible and the ones in the middle to visible.
		for (int i = 0; i < layout.size(); i++) {
			for (int a = 0; a < layout.get(i).length; a++) {
				for (int b = 0; b < layout.get(i)[a].length; b++) {
					if (i == 0 || i == 2 || i == 6 || i == 8) {
						layout.get(i)[a][b] = false;
					} else {
						layout.get(i)[a][b] = true;
					}
				}
			}
		}
		
		cells = addArray(cells, layout.get(0), 0, 0);

		//Works out how to merge the 9 separate smaller arrays into one bigger array based on how the shape is divided
		if (x3 == 5 && y3 == 5) {
			y3 = 0;
			x3 = 0;
			cells = addArray(cells, layout.get(1), x2 + x3, 0);
			x3 = 1;
			cells = addArray(cells, layout.get(2), x2*2 + x3, 0);
			x3 = 0;
			cells = addArray(cells, layout.get(3), 0, y2 + y3);
			cells = addArray(cells, layout.get(4), x2 + x3, y2 + y3);
			cells = addArray(cells, layout.get(5), x2*2 + x3, y2 + y3);
			y3 = 1;
			cells = addArray(cells, layout.get(6), 0, y2*2 + y3);
			y3 = 0;
			cells = addArray(cells, layout.get(7), x2 + x3, y2*2 + y3);
			x3 = 1;
			y3 = 1;
			cells = addArray(cells, layout.get(8), x2*2 + x3, y2*2 + y3);
		} else if (x3 == 5) {
			x3 = 0;
			cells = addArray(cells, layout.get(1), x2 + x3, 0);
			x3 = 1;
			cells = addArray(cells, layout.get(2), x2*2 + x3, 0);
			x3 = 0;
			cells = addArray(cells, layout.get(3), 0, y2 + y3);
			cells = addArray(cells, layout.get(4), x2 + x3, y2 + y3);
			x3 = 1;
			cells = addArray(cells, layout.get(5), x2*2 + x3, y2 + y3);
			x3 = 0;
			cells = addArray(cells, layout.get(6), 0, y2*2 + y3);
			cells = addArray(cells, layout.get(7), x2 + x3, y2*2 + y3);
			x3 = 1;
			cells = addArray(cells, layout.get(8), x2*2 + x3, y2*2 + y3);
		} else if (y3 == 5) {
			y3 = 1;
			cells = addArray(cells, layout.get(2), x2*2 + x3, 0);
			cells = addArray(cells, layout.get(3), 0, y2 + y3);
			cells = addArray(cells, layout.get(4), x2 + x3, y2 + y3);
			cells = addArray(cells, layout.get(5), x2*2 + x3, y2 + y3);
			cells = addArray(cells, layout.get(6), 0, y2*2 + y3);
			cells = addArray(cells, layout.get(7), x2 + x3, y2*2 + y3);
			cells = addArray(cells, layout.get(8), x2*2 + x3, y2*2 + y3);
			y3 = 0;
		} else {
			cells = addArray(cells, layout.get(2), x2*2 + x3, 0);
			cells = addArray(cells, layout.get(3), 0, y2 + y3);
			cells = addArray(cells, layout.get(4), x2 + x3, y2 + y3);
			cells = addArray(cells, layout.get(5), x2*2 + x3, y2 + y3);
			cells = addArray(cells, layout.get(6), 0, y2*2 + y3);
			cells = addArray(cells, layout.get(7), x2 + x3, y2*2 + y3);
			cells = addArray(cells, layout.get(8), x2*2 + x3, y2*2 + y3);
		}
	}
	
	/**
	 * Merges a smaller array with a bigger array starting at the given coordinates
	 * 
	 * @param cells - The main array to merge into
	 * @param cells2 - The secondary array that needs to be merged
	 * @param x - Starting width
	 * @param y - Starting height
	 * @return - The original array with the merged array in it
	 */
	private Cell[][] addArray(Cell cells[][], boolean cells2[][], int x, int y) {
		for (int a = 0; a < cells2.length; a++) {
			for (int b = 0; b < cells2[a].length; b++) {
				cells[a + x][b + y].setVisible(cells2[a][b]);
			}
		}
		return cells;
	}
	
	/**
	 * Generates the rectangle shape
	 */
	private void createRectangle() {
		for (int a = 0; a < x; a++) {
			for (int b = 0; b < y; b++) {
				cells[a][b] = new Cell(a, b, true);
			}
		}
	}
	
	/**
	 * Generates the circle shape based on the midpoint circle algorithm
	 * Reference:
	 * https://rosettacode.org/wiki/Bitmap/Midpoint_circle_algorithm#Java
	 */
	private void createCircle() {
		int centreX = x/2;
		int centreY = y/2;
		int radius = x/2;
		
		int d = (5 - radius * 4)/4;
		int x2 = 0;
		int y2 = radius;
		
		for (int i = 0; i < x; i++) {
			for (int a = 0; a < y; a++) {
				cells[i][a] = new Cell(i, a, false);
			}
		}
		
		do {
			cells[centreX + x2][centreY + y2].setVisible(true);
			cells[centreX + x2][centreY - y2].setVisible(true);
			cells[centreX - x2][centreY + y2].setVisible(true);
			cells[centreX - x2][centreY - y2].setVisible(true);
			cells[centreX + y2][centreY + x2].setVisible(true);
			cells[centreX + y2][centreY - x2].setVisible(true);
			cells[centreX - y2][centreY + x2].setVisible(true);
			cells[centreX - y2][centreY - x2].setVisible(true);
			
			if (d < 0) {
				d += 2 * x2 + 1;
			} else {
				d += 2 * (x2 - y2) + 1;
				y2--;
			}
			x2++;
		} while (x2 <= y2);
		
		int temp[][] = new int[x][y];

		for (int i = 0; i < x; i++) {
			for (int a = 0; a < y; a++) {
				if (checkHorizontal(i, a) && checkVertical(i, a)) {
					temp[i][a] = 1;
				} else {
					temp[i][a] = 0;
				}
			}
		}
		
		for (int i = 0; i < x; i++) {
			for (int a = 0; a < y; a++) {
				if (temp[i][a] == 1) {
					cells[i][a].setVisible(true);
				}
			}
		}
	}
	
	/**
	 * Checks to see if there are visible cells above and below the given coordinates
	 * 
	 * @param x - Coordinate that is being checked
	 * @param y - Coordinate that is being checked
	 * @return True if the there are visible cells above and below the given coordinate
	 */
	private boolean checkVertical(int x, int y) {
		boolean up = false;
		boolean down = false;
		
		for (int i = y; i < this.y; i++) {
			if (cells[x][i].getVisible() == true) {
				down = true;
			}
		}
		
		for (int i = y; i >= 0; i--) {
			if (cells[x][i].getVisible() == true) {
				up = true;
			}
		}
		
		return up && down;
	}
	
	/**
	 * Checks to see if there are visible cells to the left and right of the given coordinate
	 * 
	 * @param x - Coordinate that is being checked
	 * @param y - Coordinate that is being checked
	 * @return True if the there are visible cells to the left and right of the given coordinate
	 */
	private boolean checkHorizontal(int x, int y) {
		boolean left = false;
		boolean right = false;
		
		for (int i = x; i < this.x; i++) {
			if (cells[i][y].getVisible() == true) {
				right = true;
			}
		}
		
		for (int i = x; i >= 0; i--) {
			if (cells[i][y].getVisible() == true) {
				left = true;
			}
		}
		
		return left && right;
	}
	
	/**
	 * @return The width of the board
	 */
	public int getWidth() {
		return x;
	}
	
	/**
	 * @return The height of the board
	 */
	public int getHeight() {
		return y;
	}
	
	/**
	 * @return The cells array with the board layout details
	 */
	public Cell[][] getLayout() {
		return cells;
	}
	
	/**
	 * @return The name of the shape
	 */
	public String getName() {
		return name;
	}
}
